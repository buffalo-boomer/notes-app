const chalk = require("chalk");
const fs = require("fs");

const addNote = (title, body) => {
  const notes = loadNotes();
  const duplicateNote = notes.find((note) => note.title === title);
  if (!duplicateNote) {
    notes.push({ title: title, body: body });
    console.log(chalk.bgGreen.black.bold("New note added!"));
  } else {
    console.log(chalk.bgRed.black.bold("Note title taken!"));
  }

  saveNotes(notes);
};

const removeNote = (title) => {
  const notes = loadNotes();
  const keepNotes = notes.filter((note) => note.title !== title);

  if (keepNotes.length < notes.length) {
    saveNotes(keepNotes);
    console.log(chalk.bgGreen.black.bold("Your note has been deleted!"));
  } else {
    console.log(chalk.bgRed.black.bold("I cant find that one..."));
  }
};

const listNotes = () => {
  console.log(chalk.cyan.bgBlack.bold.italic("Your Notes"));
  const notes = loadNotes();
  notes.forEach((note) =>
    console.log(
      chalk.black.bgGreen("Title: " + note.title) +
        "\n" +
        chalk.bgGreen.black.inverse("----------")
    )
  );
};

const readNote = (title) => {
  const notes = loadNotes();
  const note = notes.find((note) => note.title === title);
  if (!note) {
    console.log(chalk.bgRed.white.bold("Could not find that note for you"));
  } else {
    console.log(chalk.bgGreen.black.bold.italic(note.title) + "\n" + note.body);
  }
};

const saveNotes = (notes) => {
  const dataJSON = JSON.stringify(notes);
  fs.writeFileSync("notes.json", dataJSON);
};

const loadNotes = () => {
  try {
    const dataBuffer = fs.readFileSync("notes.json");
    const dataJSON = dataBuffer.toString();
    return JSON.parse(dataJSON);
  } catch (e) {
    return [];
  }
};

module.exports = {
  addNote: addNote,
  removeNote: removeNote,
  listNotes: listNotes,
  readNote: readNote,
};
