[![Gitpod ready-to-code](https://img.shields.io/badge/Gitpod-ready--to--code-blue?logo=gitpod)](https://gitpod.io/#https://github.com/buffalo-boomer/notes-app)

Created a basic note taking app backend while playing with Node.js. Plan to
expand on this in the future and add front end functionality as well as an
actual database. Maybe with react? Still in the works though. Commits are not
necessary and will probably not be accepted but I am absolutely open to ideas or
suggestions on what I should do going forward. Thanks guys and I hope this pans
out!

# notes-app
