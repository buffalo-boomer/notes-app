const chalk = require("chalk");
const yargs = require("yargs");
const fs = require("fs");
const notes = require("./notes.js");

// Customize yargs version
yargs.version("1.1.0");

// commands needed in the app
// add, remove, read, list

// Create add command
yargs.command({
  command: "add",
  describe: "Add a new note",
  builder: {
    title: { describe: "Note title", demandOption: true, type: "string" },
    body: { describe: "Note body", demandOption: true, type: "string" },
  },
  handler(argv) {
    notes.addNote(argv.title, argv.body);
  },
});

// Create list command
yargs.command({
  command: "list",
  describe: "List all the notes",
  handler() {
    notes.listNotes();
  },
});

// Create read command
yargs.command({
  command: "read",
  describe: "Read a note",
  builder: {
    title: { describe: "Note title", demandOption: true, type: "string" },
  },
  handler(argv) {
    notes.readNote(argv.title);
  },
});

// Create remove command
yargs.command({
  command: "remove",
  describe: "Remove a note",
  build: {
    title: { describe: "Note title", demandOption: true, type: "string" },
  },
  handler(argv) {
    notes.removeNote(argv.title);
  },
});

// Must have this in order to have output to the console to confirm the stdin
yargs.parse();

// OR ____ BELOW --same method different syntax
// console.log(yargs.argv);
